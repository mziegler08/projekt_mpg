package rahmenMenue;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private final int WIDTH, HEIGHT;

	MyFrame() {
		WIDTH = 800;
		HEIGHT = 600;

	}

	// Objekte f�r die graphische Darstellung
	private Image image;
	private Graphics imageGraphics;

	public void erstelleBallImage() {
		imageGraphics.setColor(Color.CYAN);
		imageGraphics.fillOval(50, 50, 50, 50);
		imageGraphics.create();
	}

	public void initImage() {
		image = createImage(WIDTH, HEIGHT);
		imageGraphics = image.getGraphics();
	}

}
