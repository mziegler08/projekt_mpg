package rahmenMenue;

//Mit dem Stern "Wildcard" werden alle Swing Bibliotheken eingebunden
import javax.swing.*;

@SuppressWarnings("serial")
public class Rahmen extends JFrame {
	public Rahmen() {
		// neues Objekt erzeugen
		JFrame f = new MyFrame();
		
		// Titel
		f.setTitle("Rahmen mit Statuszeile");

		// Fenster schlie�en
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Fenstergr��en
		f.setSize(800, 600);
		// Fenster mittig auf dem Monitor platzieren
		f.setLocationRelativeTo(null);

		// Textfeld zur Eingabe
		final JTextArea textArea = new JTextArea();
		f.add(new JScrollPane(textArea));

		// Menue
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("Datei");
		menuBar.add(fileMenu);
		JMenu helpMenu = new JMenu("Hilfe");
		menuBar.add(helpMenu);
		f.setJMenuBar(menuBar);
		helpMenu.add(new JMenuItem("�ber das Programm"));

		// Fenster und Menue Sichtbar machen
		f.setVisible(true);
		
		k.erstelleBallImage();
		k.initImage();
}
	

	// Mainmethode
	public static void main(String[] args) {
		new Rahmen();
	}
}
