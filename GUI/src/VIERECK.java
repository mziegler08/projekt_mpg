public class VIERECK{
	
	private int x, y, a, b;
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public VIERECK(int x, int y, int a, int b){
		this.x = x;
		this.y = y;
		this.a = a;
		this.b = b;
	}
}
