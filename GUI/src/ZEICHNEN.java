import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ZEICHNEN extends JFrame {

	private static final long serialVersionUID = -7725096729718049316L;
	final int WIDTH, HEIGHT;
	// Objekte f�r die graphische Darstellung (awt und swing)
	private JPanel panel;
	private Image image;
	private Graphics imageGraphics;
	private BALL[] balls;
	private VIERECK[] targets;

	ZEICHNEN() {
		WIDTH = 800;
		HEIGHT = 600;
		// Objekte erzeugen
		balls = new BALL[10];
		targets = new VIERECK[10];
		for (int i = 0; i < 10; i++) {
			balls[i] = new BALL(200 + 20 * i, 200 + 20 * i, 33, 33, 0);
			targets[i] = new VIERECK(200 + 30 * i, 100, 33, 33);
		}
		// Fenster initialisieren
		initFrame();
		//kurzen Sleep bevor wir weiter machen, damit das Fenster initialisiert wurde
		try {
			Thread.sleep(100);
			initImage();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		renderFrame();
		// Ausgabe auf dem Bildschirm
		final Graphics panelGraphics = panel.getGraphics();
		if (panelGraphics != null) {
			panelGraphics.drawImage(image, 0, 0, null);
			panelGraphics.dispose();
		}

	}

	// Grafikausgabe - Initialisierung des Fensters
	private void initFrame() {
		setTitle("Ping and Pong");
		panel = (JPanel) getContentPane();
		panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	public void initImage() {
		// Grafiken darstellen
		image = createImage(WIDTH, HEIGHT);
		imageGraphics = image.getGraphics();
	}

	// Grafik w�hrend des Spiels
	private void renderFrame() {
		// HINTERGRUND
		imageGraphics.setColor(Color.BLACK);
		imageGraphics.fillRect(0, 0, WIDTH, HEIGHT);
		// B�lle rendern
		imageGraphics.setColor(Color.RED);
		for (int i = 0; i < 10; i++) {
			final BALL ball = balls[i];
			imageGraphics.fillOval(ball.getX(), ball.getY(), 33, 33);
		}
		// Targets rendern
		imageGraphics.setColor(Color.GREEN);
		for (int i = 0; i < 10; i++) {
			final VIERECK targ = targets[i];
			imageGraphics.fillRect(targ.getX(), targ.getY(), 25, 25);
		}
		// Ausgabe auf dem Bildschirm
		final Graphics panelGraphics = panel.getGraphics();
		if (panelGraphics != null) {
			panelGraphics.drawImage(image, 0, 0, null);
			panelGraphics.dispose();
		}
	}
}
